﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TapInput : MonoBehaviour, IPointerClickHandler
{
    public bool isTapActive;
    Image TapButton;
    public Sprite[] Icons;
    PlayerMovement player;
    public enum TapTypes
    {
        None,
        Attack,
        Duck
    }

    public TapTypes tapType;

    // Start is called before the first frame update
    void Awake()
    {
        TapButton = this.transform.GetChild(0).GetComponent<Image>();
        player = GameObject.FindObjectOfType<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
		if (!isTapActive)
		{
            TapButton.color = Color.gray;
		}

        if (isTapActive)
        {
            TapButton.color = Color.yellow;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isTapActive)
        {
            
            player.ChangeState((int)tapType);
        }
    }

    public void SetTapIcon(int i)
	{
        this.tapType = (TapTypes)i;
        TapButton.sprite = Icons[i];

    }

    public void resetIcon()
	{
        this.tapType = (TapTypes)0;
        TapButton.sprite = Icons[0];

    }
}
