﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerType : MonoBehaviour
{
    // Start is called before the first frame update

    TapInput tapInput;

    public enum TriggerTypes
	{
        None,
        Attack,
        Duck
	}

    public TriggerTypes triggerType;

    void Start()
    {
        tapInput = GameObject.FindObjectOfType<TapInput>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        tapInput.isTapActive = true;
        tapInput.SetTapIcon((int)this.triggerType);
    }

    private void OnTriggerExit(Collider other)
    {
        tapInput.isTapActive = false;
        tapInput.resetIcon();
    }
}
