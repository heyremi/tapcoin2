﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float playerSpeed;
    int s;//current state as int
    int prevstate;//previous state as int

    public enum PlayerStates
    {
        None,
        Attack,
        Duck
    }

    public PlayerStates playerstate;


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(-transform.right * Time.deltaTime);

        switch (playerstate)
        {
            case (PlayerStates.None):
                
                
            break;

            case (PlayerStates.Attack):

                if (prevstate != (int)PlayerStates.Attack)
                {
                    //do transition once;
                    print("S: ATTACK");
                }

                prevstate = (int)PlayerStates.Attack;

               

            break;

            case (PlayerStates.Duck):

                if (prevstate != (int)PlayerStates.Duck)
                {
                    //do transition once;
                    print("S:DUCK");

                }

                prevstate = (int)PlayerStates.Duck;
             
                break;

        }
    }

    public void ChangeState(int i)
    {
       // print("changed state to: " + i);
        this.playerstate = (PlayerStates)i;
    }


}
